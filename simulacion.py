from persona import Persona
from comunidad import Comunidad
import random as r
from vacunas import Vacuna

class Simulacion():
    def __init__(self, comunidad):

        self.__comunidad = comunidad
        self.lista_recuperados = []
        self.lista_muertos = []

    def crear_comunidad(self):
        #se crean los objetos tipo persona
        for x in range(self.__comunidad.num_ciu):
            persona = Persona(x, self.__comunidad)
            self.__comunidad.list_cui = persona
        self.__comunidad.crear_edad()
        self.__comunidad.asignar_enfermedad_base()

    #se revuelve la lista y se divide en grupos para asignarle 
    #familias a las personas.
    #en el siguiente proyecto se implementará el porcentaje de contagio
    #según la pertenencia a un grupo familiar
    def crear_grupos(self):
        r.shuffle(self.__comunidad.list_cui)

        lista = []
        for x in self.__comunidad.list_cui:
            lista.append(x)
            if len(lista) == 5:
                for x in self.__comunidad.list_cui:
                    if x in lista:
                        for h in lista:
                            x.familia = h
                lista = []
    
    #se revuelve de nuevo la lista y se contagia a esa porcion de la poblacion
    #y se crea una lista para contener a esa gente
    def crear_enfermos_iniciales(self):
        r.shuffle(self.__comunidad.list_cui)
        self.lista_enfermos = []
        
        for x in self.__comunidad.list_cui:
            if len(self.lista_enfermos) < self.__comunidad.num_infe:
                self.lista_enfermos.append(x)
                x.estado = False
                x.dias_enfermo = 0
                x.enfermedad = self.__comunidad.enfermedad

    #con la lista nuevamente revuelta se toma otra porcion de gente, asegurandose
    #de que esté sana
    def seleccionar_gente_sana(self):
        r.shuffle(self.__comunidad.list_cui)
        self.lista_susceptible = []
        cantidad_gente = r.randint(10,15)
        for x in self.__comunidad.list_cui:
            if len(self.lista_susceptible) < cantidad_gente:
                if x.estado:
                    self.lista_susceptible.append(x)

    def verificar_misma_familia(self):
        for i in self.lista_susceptible:
            for j in self.lista_susceptible:
                if i.run != j.run:
                    if i.familia == j.familia:
                        return True

    def verificar_gravedad(self):
        for i in self.lista_enfermos:
            if i.grave:
                self.lista_enfermos.remove(i)

    def crear_vacunas(self):
        vacunas_disponibles = self.__comunidad.num_ciu * 0.5
        self.__vacuna1 = Vacuna(vacunas_disponibles * 0.5, 1, 0)
        self.__vacuna2 = Vacuna(vacunas_disponibles * 0.3, 2, 3)
        self.__vacuna3 = Vacuna(vacunas_disponibles * 0.16, 2, 6)

    def repartir_vacunas(self):
        r.shuffle(self.__comunidad.list_cui)
        max_vacuna1 = self.__vacuna1.disponibilidad
        max_vacuna2 = max_vacuna1 + self.__vacuna2.disponibilidad
        max_vacuna3 = max_vacuna2 + self.__vacuna3.disponibilidad
        for i in self.__comunidad.list_cui:
            if self.__comunidad.list_cui.index(i) <= max_vacuna1:
                i.vacuna = self.__vacuna1
            elif self.__comunidad.list_cui.index(i) <= max_vacuna2:
                i.vacuna = self.__vacuna2
            elif self.__comunidad.list_cui.index(i) <= max_vacuna3:
                i.vacuna = self.__vacuna3
            
    def ver_gravedad(self):
        for i in self.__comunidad.list_cui:
            probabilidad_grave = r.random()
            if not i.enfermedad_base:
                probabilidad_grave = probabilidad_grave * 0.6
            if i.edad > 45:
                probabilidad_grave = probabilidad_grave + 0.15
            if i.vacuna == self.__vacuna1:
                probabilidad_grave = probabilidad_grave * 0.75
            if i.vacuna == self.__vacuna2:
                probabilidad_grave = 0
            if i.vacuna == self.__vacuna3:
                probabilidad_grave = 0
                i.inmunidad = True
        if probabilidad_grave > 0.75:
            i.grave = True

    #se hace que cada persona enferma tenga la posibilidad de interactuar con la gente contagiada
    #si se reunen, se ve la probabilidad de que esta se contagie, comprobando su inmunidad
    def juntar_gente(self):
        recorrido_inicial = len(self.lista_enfermos)
        #esto va a simular que cada persona suceptible se encontrará con
        #todos los infectados de la lista de infectados, por eso se repite 
        #según el largo de esta lista
        for x in range(recorrido_inicial):
            for i in self.lista_susceptible:
                if i.estado:
                    probabilidad_contacto = r.random()
                    if self.verificar_misma_familia():
                        probabilidad_contacto *= 2
                    if probabilidad_contacto <= self.__comunidad.prob_com_fis:
                        probabilidad_contagio = r.random()
                        if probabilidad_contagio <= self.__comunidad.enfermedad.prob_inf:
                            if not i.inmunidad:
                                i.estado = False
                                self.lista_enfermos.append(i)
                                i.dias_enfermo = 0
                                i.enfermedad = self.__comunidad.enfermedad
                                self.ver_gravedad()
                                self.verificar_gravedad()

    def verificar_muerte(self):
        for i in self.__comunidad.list_cui:
            if i.grave:
                posibilidad_muerte = r.random()
                if posibilidad_muerte > 0.9:
                    i.muerto = True
                    self.lista_muertos.append(i)
                    print(f"persona con id {i} ha muerto")

    #lleva la cuenta de los dias que una persona puede estar enferma
    #luego al llegar al ultimo dia se remueve de la lista de enfermos para
    #formar parte de la de recuperados
    def monitorear_enfermos(self):
        for x in self.lista_enfermos:
            x.dia_siguiente()
            if x.estado:
                self.lista_enfermos.remove(x)
                if x.muerto:
                    self.lista_muertos.append(x)
                else:
                    self.lista_recuperados.append(x)

    #se saca la cuenta del total de la jornada, usando las listas para saber
    #la cantidad de gente
    def total_dia(self):
        total_enfermos = len(self.lista_enfermos)
        total_recuperados = len(self.lista_recuperados)
        total_muertos = len(self.lista_muertos)
        total_sanos = 0
        for i in self.__comunidad.list_cui:
            if i.estado and not i.inmunidad:
                total_sanos += 1
        print("-"*20)
        print("el total de enfermos es: ",total_enfermos)
        print("el total de recuperados es: ",total_recuperados)
        print("el total de sanos es: ", total_sanos)
        print("el total de muertos es: ",total_muertos )
        print("-"*20)
                    