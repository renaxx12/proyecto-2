
class Vacuna():

    def __init__(self, disponibilidad, dosis, dias_siguiente_dosis):
        self.__disponibilidad = disponibilidad
        self.__cantidad_dosis = dosis
        self.__dias_siguiente_dosis = dias_siguiente_dosis
        
    @property
    def disponibilidad(self):
        return self.__disponibilidad

    @property
    def cantidad_dosis(self):
        return self.__cantidad_dosis

    @property
    def dias_siguiente_dosis(self):
        return self.__dias_siguiente_dosis